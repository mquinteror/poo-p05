package unam.poo.controlador;

import unam.poo.vista.Vista;
import unam.poo.modelo.Modelo;

public class Controlador {

    public void mostrarMensaje(){
	System.out.println("este mensaje se genero en la clase [ Controlador ]");
    }

    public static void main ( String[] args ){
	// crear una instancia de cada uno de los objetos para mostrar su mensaje
	Controlador controller = new Controlador ();
	Modelo model = new Modelo();
	Vista view = new Vista();

	controller.mostrarMensaje();
	model.mostrarMensaje();
	view.mostrarMensaje();
    }
}
